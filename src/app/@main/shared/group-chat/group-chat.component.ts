import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { DataService } from '../../../services/data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../services/chat.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.component.html',
  styleUrls: ['./group-chat.component.scss']
})
export class GroupChatComponent implements OnInit {  
  videoEnd = false;
  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session3.smil/playlist.m3u8';
  bgImages = [
    '../../../../../assets/philips/morningStage.jpg',
    '../../../../../assets/philips/morningStage.jpg',
  ]
  bgImage;
  bgmorning: boolean;

  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  room_name;
  // username = JSON.parse(localStorage.getItem('virtual')).name;
  // storage:any=JSON.parse(localStorage.getItem('virtual'));
  curr_auditorium;
  constructor(private chatService: ChatService, private _ds: DataService, private router:Router, private route:ActivatedRoute) { }
  ngOnInit(): void {
    this.route.parent.params.subscribe((params:Params)=>{
      this.curr_auditorium=params.id;
      this.room_name = 'room'+this.curr_auditorium;
    });
    // $('#t_one_groupChat').modal('show');

  }
  closegroupchat(){
    $('#t_one_groupChat').modal('hide'); 
  }
  ngAfterViewInit() {
    // this.chatGroup();
    // let event_id =this.storage.event_id;
    this.chatService.getSocketMessages().subscribe((data: any) => {
      if(data==='groupchat'){
        this.chatGroup();
      }
    });
    
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  loadData() {
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    // this.chatService.addUser(data.name, this.room_name);
    localStorage.setItem('username', data.name);
  }
  chatGroup() {
    this._ds.getGroupChattingList(this.room_name).subscribe((res:any) => {
      this.messageList = res.result;
    });
  }
  postMessage(value) {
    let yyyy: any = new Date().getFullYear();
    let mm: any = new Date().getMonth() + 1;
    let dd: any = new Date().getDate();
    let hour: any = new Date().getHours();
    let min: any = new Date().getMinutes();
    let sec: any = new Date().getSeconds();
    if (sec.toString().length !== 2) {
      sec = '0' + sec;
    } else {
      sec = sec;
    }
    if (min.toString().length !== 2) {
      min = '0' + min;
    } else {
      min = min;
    }
    if (hour.toString().length !== 2) {
      hour = '0' + hour;
    } else {
      hour = hour;
    }
    if (dd.toString().length !== 2) {
      dd = '0' + dd;
    } else {
      dd = dd;
    }
    if (mm.toString().length !== 2) {
      mm = '0' + mm;
    } else {
      mm = mm;
    }
    let created: any = `${yyyy}-${mm}-${dd} ${hour}:${min}:${sec}`;

    let data: any = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('event_id', data.event_id);
    formData.append('room_name', this.room_name);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('created', created);
    this._ds.postGroupChatting(formData).subscribe(res => {
      console.log('posting', res);
    });
    this.textMessage.reset();
  }

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    const t_one_aq:any=document.getElementById('t_one_gc');
    if(targetElement===t_one_aq){
      this.closegroupchat();
    }
  }
}

