import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AurovilleComponent } from '../@core/auroville/auroville.component';
import { DreamsComponent } from '../@core/dreams/dreams.component';
import { InauguralComponent } from '../@core/inaugural/inaugural.component';
import { IndegenousComponent } from '../@core/indegenous/indegenous.component';
import { MainlobbyComponent } from '../@core/mainlobby/mainlobby.component';
import { PuducherryComponent } from '../@core/puducherry/puducherry.component';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  { path:'', redirectTo:'lobby', pathMatch:'full' },
  { path: 'lobby', loadChildren: ()=>import('../@core/lobby/lobby.module').then(m=>m.LobbyModule)},
  { path: 'auditorium', loadChildren: ()=>import('../@core/auditorium/auditorium.module').then(m=>m.AuditoriumModule) },
  { path: 'exhibition', loadChildren: ()=>import('../@core/exhibition-hall/exhibition-hall.module').then(m=>m.ExhibitionHallModule) },
  { path: 'lounge', loadChildren: ()=>import('../@core/networking-lounge/networking-lounge.module').then(m=>m.NetworkingLoungeModule) },
  { path: 'gamezone', loadChildren: ()=>import('../@core/game-zone/game-zone.module').then(m=>m.GameZoneModule) },
  { path: 'photobooth', loadChildren: ()=>import('../@core/photobooth/photobooth.module').then(m=>m.PhotoboothModule) },
  { path: 'helpdesk', loadChildren: ()=>import('../@core/helpdesk/helpdesk.module').then(m=>m.HelpdeskModule) },
  { path: 'signaturewall', loadChildren: ()=>import('../@core/signaturewall/signaturewall.module').then(m=>m.SignaturewallModule) },
  { path: 'mainlobby', component:MainlobbyComponent },
  { path: 'lobby2', loadChildren: () => import('../@core/lobby2/lobby2.module').then(m => m.Lobby2Module) },
  { path: 'lobby3', loadChildren: () => import('../@core/lobby3/lobby3.module').then(m => m.Lobby3Module) },
  { path: 'dreams', component:DreamsComponent },
  { path: 'auroville', component:AurovilleComponent },
  { path: 'puducherry', component:PuducherryComponent },
  { path: 'indegenous', component:IndegenousComponent },
  { path: 'inaugural', component:InauguralComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
