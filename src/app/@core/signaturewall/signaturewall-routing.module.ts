import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignaturewallComponent } from './signaturewall.component';

const routes: Routes = [
  {path:'', component:SignaturewallComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignaturewallRoutingModule { }
