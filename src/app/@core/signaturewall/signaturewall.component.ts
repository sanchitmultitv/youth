import { Component, OnInit, HostListener } from '@angular/core';
import { AfterViewInit,} from '@angular/core';
import { FormControl, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { DataService } from 'src/app/services/data.service';
import { Eventid } from 'src/app/shared/eventid';
declare var $:any;

@Component({
  selector: 'app-signaturewall',
  templateUrl: './signaturewall.component.html',
  styleUrls: ['./signaturewall.component.scss']
})
export class SignaturewallComponent implements OnInit {
  textMessage = new FormControl('');
  Color = new FormControl('');

  messageList = [];
  user_name = JSON.parse(localStorage.getItem('virtual')).name;
  storage:any=JSON.parse(localStorage.getItem('virtual'));
  room_name = 'pledge';
  videoWidth = 1;
  videoHeight = 0;
  showImage = false;
  liveMsg = false; 
  camstream: any;
  constructor(private router: Router, private chatService: ChatService, private _fd: DataService) { }

  ngOnInit(): void {
    $('#chattingModal').modal('show');
  }
  ngAfterViewInit() {
    // this.getGroupChat();
    let event_id =Eventid.event_id;
    // let event_id =138;
    // this.chatService.getSocketMessages(`toujeo-${event_id}`).subscribe((data: any) => {
    //   console.log('testing', data)
    //   if(data==='groupchat'){
    //     this.getGroupChat();
    //   }
    // });   

  }
  onClickMe1() {
    this.textMessage.setValue("I pledge to make Flavedon OD Available 24 x 7 in my territory")
  // this.clickMessage1 = 'I pledge to made Flavedon OD Available 24 x 7 in my territory';
}

onClickMe2(){
  this.textMessage.setValue("I pledge to handle all objections confidently 24 x 7") 
}
onClickMe3(){
 this.textMessage.setValue("I pledge to continuously increase my Rxer base")

}
onClickMe4(){
 this.textMessage.setValue("I pledge to keep my Flavedon OD 80 mg knowledge up to date")

}
onClickMe5(){
 this.textMessage.setValue("I pledge for 12k strips PMPM by Sept for Flavedon OD")

}
onClickMe6(){
 this.textMessage.setValue("I pledge to track my Flavedon activity outcomes 24 x 7")

}
getGroupChat(){
  this._fd.groupchating('pledge').subscribe(res => {
    this.messageList = res.result; 
    this.messageList.reverse()
  });
}
@HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let chat: any = document.getElementById('chattingModal');
    if(targetElement===chat){
      this.router.navigate(['/eventhall']);
    }
  }
  closeChat(){
    this.router.navigate(['/eventhall']);
  }
  postMessage(value,color) {
    console.log(value,color)
    let yyyy: any = new Date().getFullYear();
    let mm: any = new Date().getMonth() + 1;
    let dd: any = new Date().getDate();
    let hour: any = new Date().getHours();
    let min: any = new Date().getMinutes();
    let sec: any = new Date().getSeconds();
    if (sec.toString().length !== 2) {
      sec = '0' + sec;
    } else {
      sec = sec;
    }
    if (min.toString().length !== 2) {
      min = '0' + min;
    } else {
      min = min;
    }
    if (hour.toString().length !== 2) {
      hour = '0' + hour;
    } else {
      hour = hour;
    }
    if (dd.toString().length !== 2) {
      dd = '0' + dd;
    } else {
      dd = dd;
    }
    if (mm.toString().length !== 2) {
      mm = '0' + mm;
    } else {
      mm = mm;
    }
    let colour = color;
    let created: any = `${yyyy}-${mm}-${dd} ${hour}:${min}:${sec}`;
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    // const formData = new FormData();
    // formData.append('event_id', data.event_id);
    // formData.append('room_name', this.room_name);
    // formData.append('user_name', data.name);
    // formData.append('email', data.email);
    // formData.append('chat_data', value);
    // formData.append('is_approved', '1');
    // formData.append('created', created);
    // formData.append('company','multitv');
    if((value !== '')&&(value !== null)){
      this._fd.postGroupchatOLD(value,data.name,data.email,this.room_name,created,colour).subscribe(res => {
        console.log('posting', res);
        this.getGroupChat();
      });
      this.textMessage.reset();
    }
  }
}
