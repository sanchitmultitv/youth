import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lobby3Component } from './lobby3.component';

const routes: Routes = [{ path: '', component: Lobby3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Lobby3RoutingModule { }
