import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Lobby3RoutingModule } from './lobby3-routing.module';
import { Lobby3Component } from './lobby3.component';


import { HttpClientModule } from '@angular/common/http';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [Lobby3Component],
  imports: [
    CommonModule,
    Lobby3RoutingModule,
    HttpClientModule,
    PdfmodalModule,RouterModule
  ]
})
export class Lobby3Module { }
