import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { DataService, localService } from 'src/app/services/data.service';
declare var $:any;
@Component({
  selector: 'app-helpdesk',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.scss']
})
export class HelpdeskComponent implements OnInit {
  pointers=[];
  sName;
  constructor(private _ds: DataService,private _ls:localService) { }

  ngOnInit(): void {
    // this._ls.stepUpAnalytics('click_helpdesk');
    this._ds.getSettingSection().pipe(map(data=>{
      this.pointers = data["helpdeskPointers"];
    })).subscribe();
  }
  pointerMethod(item){
    this.sName = item.title;
    if(!this.sName.toLowerCase().includes('helpdesk')){
      $('#pdf_modal').modal('show');
    }else{
      $('#welcome_chat_modal').modal('show');

    }
  }
}
