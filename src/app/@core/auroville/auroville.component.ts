import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var Clappr:any;

@Component({
  selector: 'app-auroville',
  templateUrl: './auroville.component.html',
  styleUrls: ['./auroville.component.scss']
})
export class AurovilleComponent implements OnInit {
  videosource:any;
  CPlayer:any;
  constructor() { }

  ngOnInit(): void {
  }

  // playGrowBySharingVideo(video) {
  //   this.videosource = video;
  //   let playVideo3: any = document.getElementById("video");
  //   playVideo3.play();
  //   $('#playVideo3').modal('show');
  // }
  // closeModalVideo() {
  //   let pauseVideo: any = document.getElementById("video");
  //   pauseVideo.currentTime = 0;
  //   pauseVideo.pause();
  //   this.videosource = '';

  //   $('#playVideo3').modal('hide');
  // }

  playGrowBySharingVideo(stream) {
    $('#playVideo3').modal('show');

    var playerElement = document.getElementById("player-wrapper");
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session10-p.smil/playlist.m3u8',
      // poster: poster,
      // mute: false,
      autoPlay: true,
      height: '50vh',
      width: '100%',
      playInline: true,
      disableErrorScreen: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }


  // playGrowBySharingVideo(video) {
  //   this.videosource = video;
  //   // let playVideo: any = document.getElementById("video");
  //   // playVideo.play();
  //   $('#playVideo').modal('show');
  // }
  closeModalVideo() {
    $('#player-wrapper').html("");
    
    // this.videosource = '';
    // let pauseVideo: any = document.getElementById("video");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
    // this.videosource = '';

    $('#playVideo3').modal('hide');
  }

}
