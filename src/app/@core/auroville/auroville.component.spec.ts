import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AurovilleComponent } from './auroville.component';

describe('AurovilleComponent', () => {
  let component: AurovilleComponent;
  let fixture: ComponentFixture<AurovilleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AurovilleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AurovilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
