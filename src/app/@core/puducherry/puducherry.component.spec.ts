import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PuducherryComponent } from './puducherry.component';

describe('PuducherryComponent', () => {
  let component: PuducherryComponent;
  let fixture: ComponentFixture<PuducherryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PuducherryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PuducherryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
