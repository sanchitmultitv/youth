import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lobby2Component } from './lobby2.component';

const routes: Routes = [{ path: '', component: Lobby2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Lobby2RoutingModule { }
