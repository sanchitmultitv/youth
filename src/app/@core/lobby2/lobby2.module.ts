import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Lobby2RoutingModule } from './lobby2-routing.module';
import { Lobby2Component } from './lobby2.component';

import { HttpClientModule } from '@angular/common/http';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [Lobby2Component],
  imports: [
    CommonModule,
    Lobby2RoutingModule,
    HttpClientModule,
    PdfmodalModule,RouterModule
  ]
})
export class Lobby2Module { }
