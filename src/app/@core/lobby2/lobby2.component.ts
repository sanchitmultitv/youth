
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { DataService, localService } from 'src/app/services/data.service';

/* import { Pointers } from './pointers';
 */declare var $:any;
declare var introJs;

@Component({
  selector: 'app-lobby2',
  templateUrl: './lobby2.component.html',
  styleUrls: ['./lobby2.component.scss']
})
export class Lobby2Component implements OnInit {
  isVideoPlayed = false;
  // pointers = Pointers;
  gotoPath;
  sName;
  pointers = [];
  json=[]
  lobbyImage;
  constructor(private router: Router, private _ds: DataService, private _auth:AuthService, private _ls:localService) { }

  ngOnInit(): void {
    // this._ls.stepUpAnalytics('click_lobby');
    //this._ls.stepUpAnalytics('click_helpdesk');
 
   /* this._ds.jsonFile().subscribe((res:any)=>{
    this.json=res
    console.log(this.json)
   }) */
 
  
  }

  ngAfterViewInit(){
    // this._auth.getMessages().subscribe(data=>{
    //   this.pointers = data["lobbyPointers"]; 
    //   this.startTour();
    // });
    this._auth.settingItems$.subscribe(items => {
      this.pointers = items.length?items[0]["lobbyPointers2"]:items;
       console.log(this.pointers);
       this.lobbyImage = items.length?items[0]["bgImages"].lobby2:items;
      console.log(this.lobbyImage); 
     
      // if(localStorage.getItem('tour_guide')==='start_guide')
      // this.startTour();
    });
  }
  
  pointerMethod(item){
    this.gotoPath = item.path;
    const pointerVideo:any = document.getElementById('pointerVideo');
    let sources = pointerVideo.getElementsByTagName('source');
    if(item.animation_url != null){
      // sources[0].src=item.animation_url;
      // pointerVideo.load();
      // pointerVideo.play();
      // this.isVideoPlayed = true;
      this.router.navigate(['/'+this.gotoPath]);
    } else{
      if(item.title=='helpdesk'){
        $('#welcome_chat_modal').modal('show');
      }
      else{
      this.sName = item.title;
       
      $('#pdf_modal').modal('show');
      }
    }
    // else{
    //   this.sName = item.title;
    //   $('#pdf_modal').modal('show');
    // }
  }
  skip(){
    const pointerVideo:any = document.getElementById('pointerVideo');
    pointerVideo.currentTime = 0;
    pointerVideo.pause();
    this.router.navigate(['/'+this.gotoPath]);    
  }
  endVideo(){
    this.router.navigate(['/'+this.gotoPath]);
  }

  myTour:any;
  startTour(){
    var myTour = {
      init: function () {
        myTour.injectScript("//cdn.jsdelivr.net/intro.js/0.5.0/intro.min.js");
        myTour.injectStyle("//cdn.jsdelivr.net/intro.js/0.5.0/introjs.css");
        myTour.bindEvents();
      },
      
      bindEvents: function () {
        $(document).ready(function() {
          myTour.setupTour();
            introJs().setOptions({
              nextLabel: "Next",
              prevLabel: "Back",
              skipLabel: "Exit",
              doneLabel: "Done",
              tooltipClass: "",
              exitOnOverlayClick: false,
              showStepNumbers: false
            }).start();
      });
        // $("#startTour").on("onload", function () {
        //   myTour.setupTour();
        //   introJs().setOptions({
        //     nextLabel: "Next",
        //     prevLabel: "Back",
        //     skipLabel: "Exit",
        //     doneLabel: "Done",
        //     tooltipClass: "",
        //     exitOnOverlayClick: false,
        //     showStepNumbers: false
        //   }).start(); 
        // });  
      },
      
      injectScript: function (scriptSource) {
        var injectScript = document.createElement('script');
        injectScript.setAttribute('src',scriptSource);
        document.getElementsByTagName('body')[0].appendChild(injectScript);    
      },
      
      injectStyle: function (styleSource) {
        var injectStyle = document.createElement('link');
        injectStyle.setAttribute('href',styleSource);
        injectStyle.setAttribute('rel','stylesheet');
        injectStyle.setAttribute('type','text/css');
        document.getElementsByTagName('head')[0].appendChild(injectStyle);    
      },
      
      setupTour: ()=> {
        // myTour.insertTourElement("#p1","This is step one!","left","1");
        // myTour.insertTourElement("#two","This is step two!","bottom","2");
        this.pointers.forEach(element => {
          myTour.insertTourElement("#"+element.class, element.text ,"left","1");
          
        });
      },
      
      insertTourElement: function (elementSelector, content, position, step) {
        position = typeof position !== 'undefined' ? position : "bottom";
        $(elementSelector).attr({
          "data-intro": content,
          "data-position": position,
          "data-step": step
        });
      }
    }
     
    myTour.init();
    
  }
  ngOnDestroy() {    
    localStorage.removeItem('tour_guide');
  }
}
