import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { ActivatedRoute } from "@angular/router";
import { Eventid } from "../../../shared/eventid";
declare var $: any;
import Swal from "sweetalert2";

declare var $: any;

@Component({
  selector: "app-audi-menu",
  templateUrl: "./audi-menu.component.html",
  styleUrls: ["./audi-menu.component.scss"],
})
export class AudiMenuComponent implements OnInit {
  @Output() myOutput: EventEmitter<any> = new EventEmitter();

  imag: any;
  event_id = Eventid.event_id;
  lik;
  heart;
  clap1: any;

  Leftmenu = [
    {
      name: "clap",
      img: "assets/icons/clap.png",
      audio: "assets/mp3/applause.mp3",
      count: "",
    },
    { name: "like", img: "assets/icons/like.png", audio: "like", count: "" },
    { name: "heart", img: "assets/icons/heart.png", audio: "heart", count: "" },
    // {name:'Hoot', img:'assets/icons/hoot.png', audio:'assets/mp3/hoot.mp3',count:''},

    // { name: 'whistle', img: 'assets/icons/whistle.png', audio: 'assets/mp3/whistle.mp3' },
    // { name: 'quiz', img: 'assets/icons/quiz.png', audio: null }
  ];
  Rightmenu = [
    { name: "ask question", img: "assets/icons/ask.png" },
    // { name: 'poll', img: 'assets/icons/poll.png' },
    // { name: 'group chat', img: 'assets/icons/chat-h.png' },
    // { name: 'hand raise', img: 'https://webinar.multitvsolution.com/handraise/images/raise-hand.png' }
  ];
  like: boolean;
  flag: any = 0;
  audi_id;

  constructor(private _ds: DataService, private _ar: ActivatedRoute) {}

  ngOnInit(): void {
    // this.getLikeCount2();
    this._ar.paramMap.subscribe((params) => {
      this.audi_id = params.get("id");
    });
  }

  getLikeCount2() {
    this._ds.getLikeCount().subscribe((data: any) => {
      // this.lik=res.result.like
      // this.heart=res.result.heart
      // this.clap1=res.result.clap
      this.Leftmenu.map((res: any) => {
        // console.log(res)
        if (res.name === "like") {
          res.count = data.result.like;
        }
        if (res.name === "heart") {
          res.count = data.result.heart;
        }
        if (res.name === "clap") {
          res.count = data.result.clap;
        }
        if (res.name === "Hoot") {
          res.count = data.result.Hoot;
        }
      });
      //alert(this.lik)

      console.log(this.Leftmenu, "poiuy");
    });
  }

  openRigthMenu(item) {
    if (item.includes("question")) {
      $("#ask_question_modal").modal("show");
    }
    if (item == "poll") {
      $("#poll_modal").modal("show");
    }
    if (item == "group chat") {
      $("#t_one_groupChat").modal("show");
    }
    if (item == "hand raise") {
      this.toggleHandRaise();
    }
  }
  /*  https://goapi.multitvsolution.com:7000/virtualapi/v1/user/comment/add
  
  POST
  
  
  event_id:182
  user_id:1234
  name:asd.mp4
  comment:like
  type:like_dislike
  to_id:155   //audi id 
  image:asd.jpg
  [5:59 pm, 28/12/2021] Divakar Multit: image = '' bhej dena */
  likeopen(data) {
    //  alert(data)
    if (data == "like") {
      this.imag = "assets/icons/like.png";
      this.like = true;
    }
    if (data == "clap") {
      this.imag = "assets/icons/clap.png";
      this.like = true;
    }
    if (data == "heart") {
      this.imag = "assets/icons/heart.png";
      this.like = true;
    }

    setTimeout(() => {
      this.like = false;
    }, 10000);

    // if(name=='like'){
    //   alert("ashish")
    // }

    // const formData = new FormData();
    // formData.append("event_id", this.event_id);
    // formData.append("user_id", JSON.parse(localStorage.getItem("virtual")).id);
    // formData.append("name", JSON.parse(localStorage.getItem("virtual")).name);
    // formData.append("comment", data);
    // formData.append("type", "like_dislike");
    // formData.append("to_id", this.audi_id);
    // formData.append("image", "");

    // if (data == "like" || data == "clap" || data == "heart" || data == "Hoot") {
    //   this._ds.postLike(formData).subscribe((res: any) => {
    //     if (res.code == 1) {
    //       this.getLikeCount2();
    //       console.log("post like data");
    //     }
    //   });
    // }
  }

  toggleHandRaise() {
    Swal.fire({
      position: "top",
      icon: "success",
      title: "Your request has been sent.",
      showConfirmButton: false,
      timer: 3000,
    });
    if (this.flag == 0) {
      this.flag = 1;
    } else {
      this.flag = 0;
    }
    this.handraise();
  }

  handraise() {
    let signify_userid = JSON.parse(localStorage.getItem("getdata")).id;
    const formsData = new FormData();
    formsData.append("user_id", signify_userid);
    formsData.append("flag", this.flag);
    console.log(formsData);
    this._ds.handraise(formsData).subscribe((res) => {
      console.log(res);
    });
  }

  openLeftMenu(item) {
    // alert(item)
    if (item == null) {
      $("#quiz_modal").modal("show");
    } else {
      var sound: any = document.createElement("audio");
      sound.id = "audio-player";
      sound.src = item;
      sound.type = "audio/mpeg";
      sound.preload = true;
      sound.autoplay = "";
      sound.controls = "controls";
      sound.play();
      sound.style.display = "none";
      document.getElementById("setAudio").appendChild(sound);
    }
  }
}
