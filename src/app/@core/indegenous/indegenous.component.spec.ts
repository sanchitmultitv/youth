import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndegenousComponent } from './indegenous.component';

describe('IndegenousComponent', () => {
  let component: IndegenousComponent;
  let fixture: ComponentFixture<IndegenousComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndegenousComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndegenousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
