import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $:any;

@Component({
  selector: 'app-game-zone',
  templateUrl: './game-zone.component.html',
  styleUrls: ['./game-zone.component.scss']
})
export class GameZoneComponent implements OnInit {
  gameData = [];
  bgImg;
  pointers;
  src;
  constructor(private _ds:DataService,private sanitiser: DomSanitizer, private render: Renderer2, private elementRef: ElementRef, private _auth:AuthService) { }

  ngOnInit(): void {
    this.getSettingSection();
    // this.getGamezonedata();
  }
  getSettingSection(){
    this._ds.getSettingSection().subscribe(res=>{
      this.bgImg = res['bgImages']['gamezone'];
      this.pointers=res['gamezonePointers']
      console.log(this.bgImg)
      console.log(this.pointers)
    });
    // this._auth.settingItems$.subscribe(items => {
    //   if(items.length){
    //     this.bgImg = items[0]['bgImages']['gamezone'];
    //   }
    // });
  }
  getGamezonedata(){
    this._ds.getGamezonedata().subscribe((res:any)=>{
      this.gameData = res.result;
      console.log(this.gameData)
    });
  }
  pointerMethod(item,url){
    let data = JSON.parse(localStorage.getItem('virtual')).id;
  //  alert(url)
//    ?user_id=1234
// ?user_id=1234
// ?user_id=1234
    if(item.name != "cars"){
      // alert('ok')
      this.src=this.sanitiser.bypassSecurityTrustResourceUrl(url+'?user_id='+data);
    }else{
      this.src=this.sanitiser.bypassSecurityTrustResourceUrl(url);
    }

    $('#open_game_modal').modal('show');
    
    document.getElementById("popupData").childNodes.forEach((ele:any) => {
    if(ele.id){
      document.getElementById(ele.id).remove();
    }
    }); 
    var iframe:any = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="450px";
    iframe.id="randomid";
    iframe.setAttribute("src", item.iframe_url);
    document.getElementById("popupData").appendChild(iframe);
    // const iframe:any = this.render.createElement('iframe');
    // this.render.setAttribute(this.elementRef.nativeElement, 'src', item.iframe_url)
    // this.render.appendChild(this.pdata, iframe);
    // return iframe;  
  }
  closeModal(){
    $('#open_game_modal').modal('hide');
  }
}
