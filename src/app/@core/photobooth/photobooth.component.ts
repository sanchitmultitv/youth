import { Location } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DataService, localService } from 'src/app/services/data.service';
import { Eventid } from 'src/app/shared/eventid';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
declare var $:any;
@Component({
  selector: 'app-photobooth',
  templateUrl: './photobooth.component.html',
  styleUrls: ['./photobooth.component.scss']
})
export class PhotoboothComponent implements OnInit {

  @ViewChild(CapturePhotoComponent) photocomp:CapturePhotoComponent;
  @ViewChild('videoStream', { static: true }) videoElement: ElementRef;
  showImage = false;
  cameraStream;
  bgImg;
  frameImgs = [
    {img:'assets/event/photobooth_files/photo-jacket.png'},
    {img:'assets/event/photobooth_files/photo-jacket2.png'},
    {img:'assets/event/photobooth_files/photo-jacket.png'},
    {img:'assets/event/photobooth_files/photo-jacket2.png'},
    {img:'assets/event/photobooth_files/photo-jacket.png'},
  ];
  selectedImg = this.frameImgs[0]['img'];
  constructor(private renderer: Renderer2, private _ds: DataService, private location: Location,private _ls:localService) { }

  ngOnInit(): void {

    // this._ls.stepUpAnalytics('click_photobooth');
    this._ds.getSettingSection().subscribe(res=>{
      this.bgImg = res['bgImages']['photobooth'];
    });
    // this.showPhotobooth();
  }
  showPhotobooth(){
    $('#photoboothModal').modal('show');
    this.cameraStream = null;
    this.showImage = false;
    this.startWebcam();
  }
  startWebcam(){
    // const constraints = { "video": { width: 320, height: 180, facingMode: "user" }};
    let constraints = {
      facingMode: { exact: 'environment' },
      video: {
        width: { ideal: 640 },
        height: { ideal: 360 }
      }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(this.gotMedia.bind(this)).catch(e=>{
        console.error('getusermedia() failed: '+e);
    })
  }
  theRecorder:any;
  gotMedia(stream){
    this.cameraStream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }
  dataUrl;
  img;
  capturePhoto(){
    this.showImage = true;
    let canvasRec:any = document.getElementById('canvas');
    let preview:any = document.getElementById('videobgStream');
    let context = canvasRec.getContext('2d');
    var cw = 640;
    var ch = 360;
    canvasRec.width = cw;
    canvasRec.height = ch;
    context.drawImage(preview, 0, 0, cw, ch ); 
       
    let watermark = new Image();
    let bgimg = new Image();
    context.beginPath();
    
    // bgimg.src = 'assets/event/photobooth_files/photo-jacket.png';
    bgimg.src = this.selectedImg;
    context.drawImage(bgimg, 0, 0, preview.videoWidth, preview.videoHeight);
    this.dataUrl = canvasRec.toDataURL('mime');
    this.img = canvasRec.toDataURL("image/jpeg", 0.7);
    document.querySelector('.facebook-btn').addEventListener('click', ()=>{
      window.open(`https://www.facebook.com/sharer.php?u=www.google.com&t=hey`, 'facebook',
      'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;        
    });
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let selfie:any=document.getElementById('photoboothModal');
      if(targetElement===selfie){
        this.closePhotobooth();
      }
  }
  @HostListener('keydown', ['$event']) onKeyDown(e) {
    if (e.keyCode == 27) {
      this.closePhotobooth();
    }
  }
  closePhotobooth(){
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    $('#photoboothModal').modal('hide');
  }
  downloadPic(){
    window.location.href=this.img;
    // let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    // let user_name = JSON.parse(localStorage.getItem('virtual')).name;
    // const event_id = Eventid.event_id;
    // const formData = new FormData();
    // formData.append('user_id', user_id);
    // formData.append('user_name', user_name);
    // formData.append('image', this.img);
    // formData.append('event_id', event_id);
    // this._ds.uploadCapturePic(formData).subscribe(res => {
    //   console.log('upload', res);
    // });
    
  }
  reload() {
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    this.showImage = false;
    this.startWebcam();
  }

  OpenCapturePhoto(){
    this.photocomp.openCapturePhotoModal();
  }
  scrollTime = 0;
  scrollTo = -111;
  showTopBtn = false;
  showBottomBtn = true;
  changeFrameImg(frame){
    this.selectedImg = frame.img;
  }
  moveTop(){
    this.scrollTime--;
    let frameImg:any = document.getElementById('frameImg');
    frameImg.style.transform = 'translateY('+this.scrollTo * this.scrollTime +'px)';
    frameImg.style.transition = '.3s';
    if(this.scrollTime == -1){
      this.scrollTime = 4;
      frameImg.style.transform = 'translateY('+this.scrollTo * this.scrollTime +'px)';
      frameImg.style.transition = '.3s';
    }
  }
  moveDown(){
    this.scrollTime++;
    let frameImg:any = document.getElementById('frameImg');
    frameImg.style.transform = 'translateY('+this.scrollTo * this.scrollTime +'px)';
    frameImg.style.transition = '.3s';
    console.log(this.scrollTime)
    if(this.scrollTime == this.frameImgs.length){
      this.scrollTime = 0;
      frameImg.style.transform = 'translateY('+this.scrollTo * this.scrollTime +'px)';
      frameImg.style.transition = '.3s';
    }
  }
  showingBg(){
    let bg:any = document.getElementById('bgplace');
    bg.style.transition='.3s';
    if(window.getComputedStyle(bg).getPropertyValue('transform')==='matrix(1, 0, 0, 1, -1500, 0)'){
      bg.style.transform='translate(0)';
    } else{
      bg.style.transform='translate(-1500px)';
    }
  }
}
