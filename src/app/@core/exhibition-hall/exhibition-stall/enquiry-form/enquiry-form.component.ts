import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
@Component({
  selector: 'app-enquiry-form',
  templateUrl: './enquiry-form.component.html',
  styleUrls: ['./enquiry-form.component.scss']
})
export class EnquiryFormComponent implements OnInit {
  enquiryForm:FormGroup;
  items: string[] = ['fire safety', 'security solutions', 'electrical products', 'building management system'];

  constructor(private fb:FormBuilder, private _ds: DataService) { }

  ngOnInit(): void {
    this.enquiryForm =  this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      company: ['', Validators.required],
      contact: ['', Validators.required],
      category: this.fb.array([]),
    });
    this.items.forEach(item => {
      this.addDynamicElement.push(this.fb.control(''));
    });
  }
  get addDynamicElement() {
    return this.enquiryForm.get('category') as FormArray
  }
  submitEnquiryForm(){
    console.log(this.enquiryForm.value)
    const formData = new FormData();
    formData.append('name', '');
    formData.append('email', '');
    formData.append('company', '');
    formData.append('contact', '');
    formData.append('category', '');
    this._ds.submitEnquiryForm(formData).subscribe(res=>console.log(res));
  }
  closeModal(){
    $('#enquiry_modal').modal('hide');
  }
  ischecking(event){
    const checkedEv:any = document.querySelector('#'+event+':checked');
    console.log(event, checkedEv)
    if((checkedEv!=null)||(checkedEv!=undefined)||(checkedEv!='')){

      // this.items.forEach(e=>{
      // });
    }
  }
}
