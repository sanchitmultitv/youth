import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionHallComponent } from './exhibition-hall.component';

const routes: Routes = [
  { path:'', 
    component: ExhibitionHallComponent,
    children:[
      { path:'home/:id', loadChildren:()=>import('./exhibition-home/exhibition-home.module').then(m=>m.ExhibitionHomeModule) },
      { path:'stall/:id', loadChildren:()=>import('./exhibition-stall/exhibition-stall.module').then(m=>m.ExhibitionStallModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHallRoutingModule { }
