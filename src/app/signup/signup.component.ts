import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './base.signup.component.html',
  styleUrls: ['./base.signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  bgImg;
  // isSubmitted = false;
  constructor(private router: Router,private _auth: AuthService, private fb: FormBuilder, private _ds:DataService, private http: HttpClient) { }

  ngOnInit(): void {
    this.router.navigate(['/login']);

    this.signupForm =  this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      state: ['', Validators.required],
      phone: [''],
      gender: ['', Validators.required],
      age: ['', Validators.required],
      // city: ['***', Validators.required],
      // gender: ['male', Validators.required],
      // is_founded: ['**', Validators.required],
      // policy: ['**', Validators.required],
    });
//     this._ds.getSettingSection().subscribe(res=>
//     document.getElementById("bg").style.backgroundImage = "url("+'background.jpg'+")"
//     // console.log(res['bgImages']['signup'])
// );
var countDownDate = new Date("Jan 12, 2022 10:30:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML ="GOING LIVE IN  "+ days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
  }
  submitSignup(data) {
    console.log('respone', this.signupForm.value)
    // this.isSubmitted = true;
    const formData = new FormData();
    formData.append('name', this.signupForm.value.name);
    formData.append('email', this.signupForm.value.email);
    formData.append('company', this.signupForm.value.state);
    formData.append('designation', this.signupForm.value.age);
    formData.append('mobile', this.signupForm.value.phone);
    formData.append('category', this.signupForm.value.gender);
    // formData.append('city', '***');
    // formData.append('gender', '***');
    // formData.append('is_founded', '***');
    // formData.append('tnc', '***');

 
    if (this.signupForm.valid) {
      this._auth.signupMethod(formData).subscribe((res: any) => {
        if (res.code == 1) {
          // this.isSubmitted = false;
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Registration Success!!',
            showConfirmButton: false,
            timer: 3000
          });
          data.resetForm(); 
          this.resetForm();
        } else {          
          data.resetForm();          
          this.resetForm();
          Swal.fire({
            position: 'top',
            icon: 'error',
            title: res.result,
            showConfirmButton: false,
            timer: 3000
          });
        }
        
      });
    } else {
      console.log("thisis msg")
    }
  }
  resetForm(){
    this.signupForm.patchValue({
      mobile: '***',
      city: '***',
      gender: '',
      is_founded: '**',
      policy: '**'
    });
  }
}
